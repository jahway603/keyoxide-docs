+++
title = "Adding an avatar"
+++

Let's add an avatar to your Keyoxide profile.

## Libravatar 

You can [sign up on Libravatar.org](https://www.libravatar.org/) or [run your own instance](https://wiki.libravatar.org/running_your_own/), both methods are supported.

Make sure your Libravatar account's email address is the same as the one in your OpenPGP key.

Log in to your Libravatar account and upload a profile image.

And you're done! Reload your Keyoxide profile page, it should now show your Libravatar profile image.

## Gravatar 

If you prefer to use Gravatar instead, then you can [sign up on Gravatar.com](https://gravatar.com/).

Make sure your Gravatar account's email address is the same as the one in your OpenPGP key.

Log in to your Gravatar account and upload a profile image.

And you're done! Reload your Keyoxide profile page, it should now show your Gravatar profile image.
