+++
title = "Lichess"
+++

Let's add a decentralized Lichess proof to your OpenPGP keys.

## Update the Lichess account

Log in to [lichess.org](https://lichess.org) and add a link to your profile pointing at your Keyoxide page (make sure to replace FINGERPRINT):

```
https://keyoxide.org/FINGERPRINT
// or
https://yourkeyoxide.instance/FINGERPRINT
```

If you use an email address as identifier on Keyoxide, makes sure to still add the fingerprint as such:

```
https://keyoxide.org/EMAIL#FINGERPRINT
// or
https://keyoxide.org/hkp/EMAIL#FINGERPRINT
```

## Update the PGP key

First, edit the key (make sure to replace FINGERPRINT):

```
gpg --edit-key FINGERPRINT
```

Get a list of user IDs and find the index of the one to assign the notation to:

```
list
```

Select the desired user ID (make sure to replace N):

```
uid N
```

Add a new notation:

```
notation
```

Enter the notation (make sure to replace USERNAME):

```
proof@ariadne.id=https://lichess.org/@/USERNAME
```

Save the key:

```
save
```

Upload the key to WKD or use the following command to upload the key to [keys.openpgp.org](https://keys.openpgp.org) (make sure to replace FINGERPRINT):

```
gpg --keyserver hkps://keys.openpgp.org --send-keys FINGERPRINT
```

And you're done! Reload your profile page, it should now show a verified Lichess account.