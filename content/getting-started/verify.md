+++
title = "Verifying a signature"
+++

Let's see how to verify an OpenPGP signature.

## Obtain a signature

If you already have a signature you would like to verify, great! If not, let's use the following signature for the guide:

```
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

I like pineapple.
-----BEGIN PGP SIGNATURE-----

iQJDBAEBCAAtFiEEog/Pt4tEmnyVrrtlNzZ/SvQIetEFAl70mVUPHHlhcm1vQHlh
cm1vLmV1AAoJEDc2f0r0CHrRQXIP/08uza9zOtmZXv5K+uPGVzDKwkgPgZJEezX7
6iQ358f1pjSRvYfQ5aB13k2epUHoqCKArMYu1zPqxhvLvvAvp8uOHABnr9NGL3El
u7UUgaeUNHkr0gxCKEq3p81abrrbbWveP8OBP4RyxmaFx13Xcj7mfDluiBHmjVvv
WU09EdH9VPlJ7WfZ+2G2ZZDHuE5XiaeP7ocugTxXXLkp33zwpDX0+ZuCIXM6fQGe
OccSffglFPdNBnfasuuxDWxTQPsEbWGOPJV+CAPmBDeApX+TBF9bovO3hw4Uozk2
VT7EAy8Hb0SOrUb3UNGxzoKv++5676IxyB4JXX0Tr9O4ZxhO8o9pEEHwirtn/J1+
MWven4gVlWM/6bMeUqx6ydyNc2nqF5059yfRmwGMlp09x82G4x1bcf6aDZ+5njDG
fS5T2OpXRIkZHJx8BhmZjsxiDR0KV44zwHpt06+96ef3EDWB0BcP6M+a5Rtc33zf
irRmQd2M6RLyXCYtdGIiiAFRuomw802U4F0P4LwVrZdbGA6ObqBv1k8BUFCMbMz8
Ab4hF7kO4z0Vh3JaKzcHey0pOzdNCPpAHZ51sAoAnFDM4PdMBgQxxVweCMu4KYMZ
FN8sNn42oY/b7gDmwCelVhgD+rvUn/a8+B7CDmCp+wIquyrjrTt00voATcb+ZPMJ
pTXJ/NcM
=rqTX
-----END PGP SIGNATURE-----
```

Copy the above signature.

## Verify the signature

Open the [keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d](https://keyoxide.org/9f0048ac0b23301e1f77e994909f6bd6f80f485d) page, click the **Verify signature** button and paste the signature in the corresponding field.

Keyoxide lets you know the signature was verified and signed by that public key.

## Verify the signature against a different public key

Sometimes, you want to know if a public key belonging to a specific person was used to create a signature.

In this example, let's figure out if the message was signed by Yarmo's public key or a different public key (perhaps pretending to be Yarmo's public key).

Open the [keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b](https://keyoxide.org/3637202523e7c1309ab79e99ef2dc5827b445f4b) page, click the **Verify signature** button and paste the signature in the corresponding field.

Keyoxide lets you know the signature was NOT signed by the profile's key. Careful with what the signed message says, it may not be written by whom you thought it was!

## Signing your own messages

If you'd like to sign messages using PGP, you must first learn the fundamentals of PGP and how to generate and manage your own keypair.
