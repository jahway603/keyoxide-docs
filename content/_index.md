+++
title = "Docs"

template = "section.html"
page_template = "page.html"

redirect_to = "/getting_started/"
+++

Go to [here](/getting-started/).