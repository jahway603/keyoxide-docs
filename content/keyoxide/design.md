+++
title = "Design"
+++

## Logo

{{ logo_128() }}

Download: [logo_circle_1024.png](/logo_circle_1024.png)

## Colors

### Light theme

<span class="color-showcase dark" style="background-color:#6855c3">#6855c3</span> Header text

<span class="color-showcase dark" style="background-color:#086191">#086191</span> Link text

<span class="color-showcase" style="background-color:#ffffff">#ffffff</span> Background

<span class="color-showcase" style="background-color:#f9f8fb">#f9f8fb</span> Card background

<span class="color-showcase" style="background-color:#ddd9f2">#ddd9f2</span> Card border

<span class="color-showcase" style="background-color:#eeecf8">#eeecf8</span> Claim card background

<span class="color-showcase dark" style="background-color:#7868ca">#7868ca</span> Button background

<span class="color-showcase dark" style="background-color:#479438">#479438</span> Verification success

<span class="color-showcase dark" style="background-color:#d6705c">#d6705c</span> Verification failure

### Dark theme

<span class="color-showcase" style="background-color:#cdc6eb">#cdc6eb</span> Header text

<span class="color-showcase" style="background-color:#43b0ea">#43b0ea</span> Link text

<span class="color-showcase dark" style="background-color:#121212">#121212</span> Background

<span class="color-showcase dark" style="background-color:#191720">#191720</span> Card background

<span class="color-showcase dark" style="background-color:#26203a">#26203a</span> Card border

<span class="color-showcase dark" style="background-color:#26203a">#26203a</span> Claim card background

<span class="color-showcase dark" style="background-color:#7868ca">#7868ca</span> Button background

<span class="color-showcase" style="background-color:#7ac76b">#7ac76b</span> Verification success

<span class="color-showcase dark" style="background-color:#d6705c">#d6705c</span> Verification failure
